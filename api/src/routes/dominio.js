const axios = require('axios')
const express = require('express');
const router = express()
const url = "https://api.mercadolibre.com/sites/MLA/domain_discovery/search" // ?q=celular


const dominioCategorie = (req, res) => {
  console.log(req)
  const domainProduct = req.query
  axios.get(url + `?q=${domainProduct}`)
    .then(response => {
      return res.status(200).json(response.data)
    })
    .catch(function (error) {
      res.status(400).json({ error })
      console.log('error del then dominio', error)
    })

}


router.get('/', dominioCategorie)
module.exports = router