const { Router } = require('express');
const router = Router();
const allCategories = require('./allCategories.js')
const categorie = require('./categorie')
const dominio = require('./dominio')
const nameProduct = require('./forName')

// poner todas la rutas
router.use("/categories", allCategories);
router.use("/categorie", categorie);
router.use("/dominio", dominio);
router.use("/nameProduct", nameProduct);

module.exports = router;
