const axios = require('axios')
const express = require('express');
const router = express()
const url = "https://api.mercadolibre.com/categories"

const getCategorie = (req, res) => {
  const idCategorie = req.query.id;
  axios.get(url + `/${idCategorie}`)
    .then(response => {
      return res.status(200).json(response.data)
    })
    .catch(function (error) {
      console.log('error del then categorie', error)
      return res.status(400).json(error)
    })
}

router.get('/', getCategorie)
module.exports = router