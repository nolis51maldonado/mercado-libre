const axios = require('axios')
const express = require('express');
const router = express()
const url = "https://api.mercadolibre.com/sites/MLA/categories"


const getAllCategories = async (req, resp) => { // categorias - cargar en bd
  axios.get(url)
    .then(response => {
      console.log('response', response)
      return resp.status(200).json(response.data)
    })
    .catch(function (error) {
      resp.status(400).json({ error })
      console.log('error del then allCategories', error)
    })
}

router.get('/', getAllCategories)
module.exports = router