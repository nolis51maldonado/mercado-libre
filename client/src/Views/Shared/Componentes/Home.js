import React, { useEffect } from 'react';
import { getAllCategories } from '../../../Redux/Actions/Actions';
import { useDispatch, useSelector } from 'react-redux';
import { Outlet } from 'react-router-dom';
import Cards from '../Componentes/Cards/Cards'


export default function Home ({id}){
    const dispatch = useDispatch()
    const allCategories = useSelector(initialState => initialState?.getAllCategories)
    

    useEffect(()=>{
        dispatch(getAllCategories())
    },[])

    
    
    return (<Cards />)
        

      
   
}