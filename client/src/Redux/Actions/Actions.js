import axios from "axios";


export const GET_ALL_CATEGORIES = "GET_ALL_CATEGORIES"


export function getAllCategories() {

    return async function (dispatch){

        axios.get("http://localhost:3001/categories")
        .then(res =>{
            console.log(res)
            dispatch({
                type: GET_ALL_CATEGORIES,
                payload:res.data
            }) 
        } )
        .catch(err=>{
            return (err, "Error en action getAllCategories")
        })
       
    }
}